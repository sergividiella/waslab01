package wallOfTweets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Locale;
import java.util.Vector;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WoTServlet
 */
@WebServlet("/")
public class WoTServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Locale currentLocale = new Locale("en");
	String ENCODING = "ISO-8859-1";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WoTServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			Vector<Tweet> tweets = Database.getTweets();

			if (request.getHeader("Accept").compareTo("text/plain") == 0) {
				printTextResult(tweets, request, response);
			} else {
				printHTMLresult(tweets, request, response);
			}
		}

		catch (SQLException ex) {
			throw new ServletException(ex);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// This method does NOTHING but redirect to the main page
		try {

			String action = request.getParameter("action");
			if (action != null && action.compareTo("Delete") == 0) {
				Cookie cookies[] = request.getCookies();
				String id = request.getParameter("id");

				for (Cookie cookie : cookies) {
					if (cookie.getName().startsWith("tweetid") && dexifrar(cookie.getValue()).compareTo(id) == 0)
						Database.deleteTweet(Long.parseLong(id));
				}

				if (request.getHeader("Accept").compareTo("text/plain") != 0)
					response.sendRedirect(request.getContextPath());

			} else {

				String author = request.getParameter("author");
				String text = request.getParameter("tweet_text");
				long id = Database.insertTweet(author, text);
				int num = request.getCookies() != null ? request.getCookies().length : 0;
				response.addCookie(new Cookie("tweetid" + num, xifrar(Long.toString(id))));
				if (request.getHeader("Accept").compareTo("text/plain") == 0)
					response.getWriter().print(id);
				else
					response.sendRedirect(request.getContextPath());

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printHTMLresult(Vector<Tweet> tweets, HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
		DateFormat timeFormatter = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocale);
		res.setContentType("text/html");
		res.setCharacterEncoding(ENCODING);
		PrintWriter out = res.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head><title>Wall of Tweets</title>");
		out.println("<link href=\"wallstyle.css\" rel=\"stylesheet\" type=\"text/css\" />");
		out.println("</head>");
		out.println("<body class=\"wallbody\">");
		out.println("<h1>Wall of Tweets</h1>");
		out.println("<div class=\"walltweet\">");
		out.println("<form method=\"post\">");
		out.println("<table border=0 cellpadding=2>");
		out.println("<tr><td>Your name:</td><td><input name=\"author\" type=\"text\" size=70></td><td></td></tr>");
		out.println(
				"<tr><td>Your tweet:</td><td><textarea name=\"tweet_text\" rows=\"2\" cols=\"70\" wrap></textarea></td>");
		out.println("<td><input type=\"submit\" name=\"action\" value=\"Tweet!\"></td></tr>");
		out.println("</table></form></div>");
		String currentDate = "None";
		for (Tweet tweet : tweets) {
			String messDate = dateFormatter.format(tweet.getDate());
			if (!currentDate.equals(messDate)) {
				out.println("<br><h3>...... " + messDate + "</h3>");
				currentDate = messDate;
			}
			out.println("<div class=\"wallitem\">");
			out.println("<h4><em>" + tweet.getAuthor() + "</em> @ " + timeFormatter.format(tweet.getDate()) + "</h4>");
			out.println("<p>" + tweet.getText() + "</p>");
			out.println("<p><form method=\"post\">");
			out.println("<input type=\"hidden\" name=\"id\" value=" + tweet.getTwid() + ">");
			out.println("<td><input type=\"submit\" name=\"action\" value=\"Delete\"></td></tr></form></p>");
			out.println("</div>");

		}
		out.println("</body></html>");
	}

	private void printTextResult(Vector<Tweet> tweets, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// TODO Auto-generated method stub
		DateFormat dateFormatter = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.DEFAULT, currentLocale);
		response.setContentType("text/plain");
		response.setCharacterEncoding(ENCODING);
		PrintWriter out = response.getWriter();
		for (Tweet tweet : tweets) {
			String messDate = dateFormatter.format(tweet.getDate());
			out.println("tweet #".concat(tweet.getTwid() + ": ").concat(tweet.getAuthor() + ": ")
					.concat(tweet.getText() + " [").concat(messDate + "]"));

		}

	}

	private String xifrar(String text) {

		try {

			Cipher cipher = getCipher(true);
			byte[] cipherText = cipher.doFinal(text.getBytes("UTF-8"));
			return Base64.getEncoder().encodeToString(cipherText);

		} catch (Exception e) {
			throw new RuntimeException("Error occured while encrypting data", e);
		}

	}

	private String dexifrar(String text) {

		try {

			Cipher cipher = getCipher(false);
			byte[] cipherText = cipher.doFinal(Base64.getDecoder().decode(text));
			return new String(cipherText);

		} catch (Exception e) {
			// Error de format de cookies anteriors es retorna el text per a no tornar un
			// http 500 al client
			return text;

		}

	}

	private Cipher getCipher(boolean xifrar) {

		final byte[] IV_PARAM_AES = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,
				0x0E, 0x0F, 0x00 };

		IvParameterSpec iv = new IvParameterSpec(IV_PARAM_AES);
		String method = "AES";
		String hash = "SHA-256";
		String block = "CBC";
		String padding = "PKCS5Padding";
		int keySize = Integer.parseInt("192");
		String pass = "UTF-8";
		int cmode = xifrar ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE;

		MessageDigest md;

		try {
			md = MessageDigest.getInstance(hash);
			byte[] clau_hash = md.digest(pass.getBytes());
			clau_hash = Arrays.copyOf(clau_hash, keySize / 8);
			SecretKey key = new SecretKeySpec(clau_hash, method);

			Cipher c = Cipher.getInstance(method + "/" + block + "/" + padding);
			c.init(cmode, key, iv);
			return c;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println();
			System.exit(1);
		}
		return null;

	}

}
