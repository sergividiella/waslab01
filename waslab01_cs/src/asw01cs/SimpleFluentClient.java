package asw01cs;

import org.apache.http.HttpHeaders;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

//This code uses the Fluent API

public class SimpleFluentClient {

	private static String URI = "http://localhost:8080/waslab01_ss/";

	public final static void main(String[] args) throws Exception {

		/* Insert code for Task #4 here */
		String id = Request.Post(URI).addHeader(HttpHeaders.ACCEPT, "text/plain")
				.bodyForm(Form.form().add("author", "Aitor").add("tweet_text", "Bon partit ahir!").build()).execute()
				.returnContent().asString();
		System.out.println(id);
		System.out.println("Tweets:");
		System.out.println(Request.Get(URI).addHeader(HttpHeaders.ACCEPT, "text/plain").execute().returnContent());

		/* Insert code for Task #5 here */
		Request.Post(URI).addHeader(HttpHeaders.ACCEPT, "text/plain")
				.bodyForm(Form.form().add("action", "Delete").add("id", id).build()).execute();
		System.out.println("Tweet " + id + ": deleted");
	}
}
